#lang pollen
◊define[text-black]{#333}
◊define[text-gray]{#666}
◊define[serif]{font-family: "Noto Serif", serif}
◊define[sans]{font-family: "Overpass", sans-serif}

body {
    ◊|sans|;
    font-weight: 400;
    font-size: 12pt;
}

@media print {
    .no-print,
    .no-print * {
        display: none;
    }
    @page {
        size: A4;
        margin: 1rem 2rem 1.3rem 2rem;
    }
}

@media screen {
    body {
        margin: 1rem 15% 1rem 15%;
        background-color: #f3f3f3;
        color: ◊|text-black|;
    }
}

a {
    transition-duration: 500ms;
    text-decoration: none;
    color: inherit;
}
a:hover{
    background-color: #ddd;
}


.char {
    margin-top: 3rem;
    margin-bottom: 0;
}
h1, h2, h3, h4 {
    margin-top: 1.6rem;
    margin-bottom: 0.8rem;
}
h1 { font-size: 16pt; }
h2 { font-size: 14pt; }
h3 {
    margin-top: 3rem;
}
h4 {
    margin-bottom: 0.8rem;
    margin-top: 2rem;
}
h5 {
    font-size: 100%;
    text-transform: uppercase;
    margin-bottom: 0;
    color: ◊|text-gray|;
}

blockquote {
    border-left: 0.2rem solid ◊|text-gray|;
    margin-top: 0;
    margin-left: 0;
    padding-left: 1rem;
    font-family: monospace;
    font-size: 14pt;
}

p {
    margin-top: 0.8rem;
    margin-bottom: 0.8rem;
}

.dialog {
    ◊|serif|;
}

.page-break {
    break-before: page;
}

.lyrics-title {
    margin-bottom: 0;
}

.name { font-weight: 700; }
.description { margin-bottom: 1rem; }

.list-not-done {
    list-style-type: circle;
}

.list-done {
    list-style-type: disc;
}

.enter-leave {
    margin-top: 2rem;
    font-weight: 700;
    ◊|serif|;
}
.enter-leave.leave {
    text-align: right;
}

.action {
    font-style: italic;
    ◊|sans|;
    /* text-align: center; */
}

.char {
    text-align: center;
    break-after: avoid;
}

.page-number {
    margin-bottom: 1.5rem;
    font-family: monospace;
}
