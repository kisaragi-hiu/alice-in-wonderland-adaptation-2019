# Schedule

## Week 1 (20190219 ~ 20190225)
- Choose roles
- Distribute works
- Finish script draft

### DONE

- script 20%

## Week 2 (20190226 ~ 20190304)
- Buy materials (scenery, props, costume)
- Edit script
- Memorize script / plot

### DONE

- Script 100%

## Week 3 (20190305 ~ 20190311)
- [x] Decide characters
- finalize script
- Make scenery, props, costumes
- Create acting, singing, dancing
- Memorize script / plot

## Week 4 (20190312 ~ 20190318)
- Make scenery, props, costumes
- Memorize script / plot
- Tweak pronunciation

## Week 5 (20190319 ~ 20190325)
- Rehearsal
- Finish scenery, props, costumes, acting, singing, dancing

## Week 6 (20190326 ~ 20190401)
- Rehearsal
- Improve fluency

## 20190402
- Performance
