# Alice in Wonderland Adaptation

A stage play adaptation of Alice’s Adventures in Wonderland.

This is the long version that's totally unsuitable for *four* people.

An e-book version of the original work can be found at <http://www.gutenberg.org/ebooks/11>.

[![CC0](https://licensebuttons.net/p/zero/1.0/88x31.png)](https://creativecommons.org/publicdomain/zero/1.0/)

This work is licensed [CC0](https://creativecommons.org/publicdomain/zero/1.0/). Zero rights reserved.
