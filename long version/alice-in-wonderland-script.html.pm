#lang pollen
◊(require racket/dict)
◊title{Alice’s adventures in wonderland adaptation script}

◊note{◊link["http://www.gutenberg.org/ebooks/11"]{Original text (public domain)}}

◊current-characters{
◊hasheq[
'sister "Alice's sister"
'alice "Alice"
'rabbit "White Rabbit"
'caterpillar "Caterpillar"
'cheshire-cat "The Cheshire Cat"
'mouse "Mouse"
'march-hare "March Hare"
'gardener-seven "Gardener: Seven"
'gardener-five "Gardener: Five"
'queen "Queen of Hearts"
]
}

◊h2{Environments}

◊ul{
◊environment['home]{Alice's home}
◊environment['hole]{Rabbit hole}
◊environment['leaves]{After falling}
◊environment['hallway]{The Hallway}
◊environment['tears-lake]{Pool of tears}
◊environment['woods]{The Woods}◊; where the mushroom was given
◊environment['rabbit-house]{Rabbit's house}
◊environment['march-hare-tea-party]{The tea party}
◊environment['garden]{Queen's garden}
◊environment['trial]{the Trial}
}

◊section{Notes}

Needs:

◊ul{
◊li{Dancing}
◊li{Singing}
}

18 ~ 22 minutes.

Everyone must have about the same number of lines. A person can have more than 1 roles.

No narrator.

◊section{Characters}

◊ul{
◊li{Alice (has a ◊i{lot} of lines)}
◊li{Queen of Hearts}
◊li{Gardener: Five}
◊li{Gardener: Seven}

◊li{Caterpillar (◊i{gives mushroom})}

◊li{Mouse (tea party guest; appears in lake of tears)}

◊li{The Cheshire Cat}

◊li{March Hare (tea party host) (= Rabbit)}
◊li{White Rabbit}

◊li{Alice's sister (has like 3 lines)}
}

◊section{Script}

◊scene{Next to a field, besides ◊character['alice]'s house. A rabbit hole is at the edge of the field.}

Alice is sitting on a chair, by her sister.

◊char['sister]

◊action{Read a book.}

◊char['alice]

◊action{Doze off.}

◊char['rabbit]

◊action{Take a watch out of a pocket.}

◊dialog{Oh dear! I'm going to be late!}

◊action{Run towards the rabbit hole}

◊leave['rabbit]

◊char['alice]

◊dialog{How curious, that rabbit has a watch!}

◊dialog{I wonder where it's going… Hey, where are you going?}

◊action{Chase after ◊character['rabbit] across the field.}
◊action{Jump into the rabbit hole.}
◊action{Chase after ◊character['rabbit] into the rabbit hole.}

◊scene{Inside the rabbit hole.}

The rabbit hole started like a tunnel, but then it suddenly turned downwards. ◊character['alice] had no time before she found herself falling down a deep well.

◊char{Stage}

◊action{Show the shelves.}

◊char['alice]

◊action{Look down below.}
◊dialog{It's too dark down below! I can't make out anything!}

◊dialog{Curious how slow I'm falling…}

◊action{Look around. Grab a jar from the shelves.}
◊dialog{I wonder what is in this jar?}
◊action{Open the jar.}
◊dialog{There's nothing in it!}
◊action{Put the jar back, a few spaces below the original position.}

◊char{Stage}

◊action{Another set of shelf items.}

◊char['alice]

◊dialog{I wonder how many miles have I fallen by now? I must be getting somewhere near the center of the earth, but then I wonder what Latitute or Longitude have I got to?}

There was nothing else to do, so Alice kept on talking.

◊dialog{Maybe I will fall right ◊em{through} the earth! "Excuse me, ma'am, is this New Zealand or Australia?"}

◊char{Stage}

◊action{Another set of shelf items.}

◊char['alice]

◊action{Begin to doze off.}
◊;dialog{I wonder what will happen now that Britain has crashed out of the EU?}


◊scene{A corner facing the audience. A dry leaf stash on one side, doors on the other.}

◊char['rabbit]
◊enter['rabbit]
◊dialog{Oh my ears, I'm out of time!}

◊char['alice]
◊action{Fall onto stash of dry leaves.}
◊action{Jump up, look at ◊character['rabbit]}

◊char['rabbit]
◊action{Run past the corner}
◊leave['rabbit]

◊char['alice]
◊action{Chase after ◊character['rabbit]}

◊scene{Hallway}

A few doors are in the hallway. There's a table in the center, and a knee-high door.

◊char['alice]
◊dialog{Where is the rabbit, and where am I?}
◊action{Try to open every door. Realize they're all locked.}
◊dialog{How am I ever going to get out of here?}

◊action{Look at table.}
◊dialog{There is a key here!}
◊action{Try the key on every door; fail.}
◊action{Notice the small door.}
◊dialog{I can't open any of these doors… what about this small one?}
◊action{Try the key on knee-high door.}
◊dialog{It opens! But I can't get through…}
◊action{Close it.}

◊action{Look at table again.}
◊dialog{I'm fairly sure this bottle wasn't here before. It tells me to drink it…}
◊dialog{Maybe if I drink it I'll magically shrink to the right size or something!}
◊action{Put key on table, then drink the potion.}

◊scene{Grow. Table and other doors now tiny.}
◊char['alice]
◊dialog{Oh no, that did the complete opposite of what I want…!}
◊action{Cry.}

◊char['rabbit]
◊action{Walk quickly through the hallway.}
◊dialog{Oh, I'm really getting late here…}

◊char['alice]
◊dialog{◊note{low and dark voice} Sir, could I please…}

◊char['rabbit]
◊dialog{What in the…!}
◊action{Scared by ◊character['alice], sprint away}
◊action{Drop pair of gloves}
◊leave['rabbit]

◊char['alice]
◊dialog{Hey, sir! Your gloves…}
◊dialog{◊note{crying voice} Oh, how helpless am I, stranded in an empty hallway!}
◊sing{
Good girl Alice, be ashamed
for crying in this way, in this hallway
Hallway without exit, what could I do
Growing into a monster, scaring away the hope
Good girl Alice, be ashamed
for crying in this way, in this hallway
}

◊scene{After singing, ◊character['alice] shrunk; lake of tears}
◊char['alice]
◊dialog{◊note{in joy} I'm shrinking!}
◊dialog{Wait, this water is getting deeper… where did all this water come from?}
◊action{Swim towards the land.}
◊action{Encounter ◊character['mouse].}
◊dialog{Hey, sir! Could you tell me if this lake was here earlier?}

◊char['mouse]
◊action{Try to stay afloat.}
◊dialog{Do you not know the legend of the crying giant? A long time ago, a giant cried and created this lake. Now go away and mind your own business!}
◊leave['mouse]

◊char['alice]
◊dialog{How rude…}
◊dialog{The giant must've been me, but I wonder why was it "a long time ago"?}
◊action{Leave lake of tears.}

◊scene{Next to lake of tears}
◊char['rabbit]
◊action{Look around.}
◊dialog{Oh my dear Lord, Goddess, and Thor, She's gonna get me executed! Where could I have dropped them?}

◊char['alice]
◊dialog{He must be looking for the gloves he dropped in the hallway!}
◊action{Look around.}
◊dialog{Wait, the hallway is gone!}

◊char['rabbit]
◊action{Notice ◊character['alice].}
◊dialog{Wait, why are you out here, Mary Ann? Run home now and fetch me a pair of gloves! Go!}
◊action{Point towards the woods.}

◊char['alice]
◊action{Run towards the woods.}
◊dialog{He must've took me for his maid. Either way, I'd better get the gloves for him, if I can find them.}

◊scene{Room in ◊character['rabbit]'s home.}
A door, a window, a pair of gloves on a table.

◊char['alice]
◊dialog{I've checked the doors, it said "W. Rabbit" alright.}
◊action{Look at table.}
◊dialog{Ah, here's the pair of gloves!}

◊char['rabbit]
◊action{Run towards his house.}
◊dialog{◊note{Shouting from background.} Mary Ann! I said to fetch me the pair of gloves, why haven't you done it?}

◊char['alice]
◊dialog{I have the gloves, but the Rabbit is mad…}

◊char['rabbit]
◊dialog{◊note{Shouting in background} I don't need excuses, get in there and fetch them for me!}

◊char['alice]
◊action{Grab the gloves and run out.}

◊scene{Outside of ◊character['rabbit]'s house.}
◊char['alice]
◊action{Attempt to give the gloves to ◊character['rabbit].}

◊char['alice]
◊dialog{Um, excuse me, I got the gloves…}

◊char['rabbit]
◊dialog{Who the hell are you? Why do you have my gloves? Give them back!}

◊char['alice]
◊dialog{Sorry!}
◊action{Drop pair of gloves. Run into the woods.}

◊scene{In the woods.}

Trees. Flowers. Caterpillar behind a huge mushroom.

There's a small mushroom that can be picked up.

◊char['alice]
◊action{Walk up to mushroom.}
◊dialog{What is this…?}
◊action{Stand tiptoe and peak over the mushroom.}
◊action{Stare at ◊character['caterpillar].}

◊char['caterpillar]
◊action{Stare at ◊character['alice] for a few seconds.}
◊dialog{Who are you?}

◊char['alice]
◊dialog{I'm like myself but not really myself—}

◊char['caterpillar]
◊dialog{I do not understand a word you're saying.}

◊char['alice]
◊dialog{Um, so I fell into this world and kept shrinking and growing, and I don't really have any idea—}

◊char['caterpillar]
◊dialog{What size do you want to be?}

◊char['alice]
◊dialog{I'm not too particular about it...}

◊char['caterpillar]
◊action{Silence for several seconds.}
◊dialog{One side will make you grow taller, the other will make you grow shorter.}

◊char['alice]
◊dialog{One side of what?}

◊char['caterpillar]
◊dialog{Of the mushroom.}
◊action{Points to mushroom.}
◊leave['caterpillar]

◊char['alice]
◊action{Pick up mushroom.}
◊dialog{I wonder what that was all about… I guess I'll pick this up in case I need it.}
◊dialog{Now, where should I go?}
◊dialog{Let me just go this way, I guess…}

◊scene{Cheshire cat}

Woods. ◊character['cheshire-cat] standing next to a tree.

◊char['alice]
◊action{Appear to be lost.}
◊action{Walk near ◊character['cheshire-cat].}
◊dialog{Excuse me, do you know which way I ought to go from here?}

◊char['cheshire-cat]
◊dialog{◊note{smile} Good evening, Alice. That—}

◊char['alice]
◊dialog{Wait, why do you know my name?}

◊char['cheshire-cat]
◊dialog{I'm a Cheshire cat, that's why.}
◊dialog{◊note{Smile further.} That depends a great deal on where you want to get to.}

◊char['alice]
◊dialog{I don't care where I—}

◊char['cheshire-cat]
◊dialog{Then it doesn't matter which way you go.}

◊char['alice]
◊dialog{Well, okay — What sort of people live around here?}

◊char['cheshire-cat]
◊dialog{◊note{Point towards audience} In that direction, lives a Hatter; ◊note{Point towards edge of stage} in that direction, lives a March Hare. Visit whomever you like: they're both mad.}

◊char['alice]
◊dialog{But I don't want to meet mad people.}

◊char['cheshire-cat]
◊dialog{There's no helping that. We're all mad here. I'm mad. You're mad.}

◊char['alice]
◊dialog{How am ◊i{I} mad?}

◊char['cheshire-cat]
◊dialog{You must be, else you wouldn't be here.}
◊dialog{Would you like to meet the Queen today?}

◊char['alice]
◊dialog{I think I'd like to, but I haven't been invited yet.}

◊char['cheshire-cat]
◊dialog{Well, you'll see me there.}
◊action{"Vanish", or walk behind a tree.}

◊char['alice]
◊action{Walk towards March Hare's home.}

◊scene{March Hare's Tea Party}

Some trees in the background. A door in one of the trees. 4-seated table in the middle. ◊character['march-hare] and ◊character['mouse] sitting together. March Hare has a watch.

◊char['alice]
◊action{Walk towards the table.}
◊dialog{Oh, this must be ◊character['march-hare]'s home. They're having a tea party!}

◊char['mouse]
◊dialog{There's no room! Go away!}

◊char['alice]
◊dialog{There ◊i{is} room!}
◊action{Sit in front of ◊character['march-hare].}

◊char['march-hare]
◊dialog{◊note{In an encouraging tone.} Have some wine.}

◊char['alice]
◊action{Look around.}
◊dialog{I don't see any wine.}

◊char['march-hare]
◊dialog{There isn't any.}
◊action{Look at the watch.}

◊char['alice]
◊dialog{Then it wasn't very civil of you to offer it.}

◊char['march-hare]
◊dialog{It wasn't very civil of you to sit down without being invited.}

◊char['mouse]
◊dialog{Why don't you just shut up if you really insist on staying?}

◊char['march-hare]
◊action{Wait for a few seconds.}
◊action{Look at the watch.}

◊char['alice]
◊action{Lean over to ◊character['march-hare].}
◊dialog{◊note{In response to March Hare looking at the watch} What a funny watch! It doesn't tell the time at all!}

◊char['march-hare]
◊dialog{Why should it? Does your watch tell you what year it is?}

◊char['alice]
◊dialog{Of course not, but that's—.}

◊char['march-hare]
◊dialog{Exactly.}

◊char['alice]
◊dialog{But that's nonsense!}

◊char['mouse]
◊dialog{You shouldn't complain after rudely appearing out of nowhere.}

◊char['alice]
◊dialog{What?}
◊action{Get up, walk away.}

◊dialog{◊note{Next to a tree.} I'll never go there again. It's the stupidest tea party I've ever been to!}
◊action{Notice door in tree.}
◊dialog{Why is there a door in a tree? That's curious!}
◊action{Open door.}

◊scene{Revisiting hallway.}

Same hallway as scene 4.

◊char['alice]
◊dialog{This is the same hallway as before! I'll manage myself better this time.}
◊action{Grab the key, pull the smaller side of mushroom.}
◊action{Open small door.}

◊scene{Garden.}
◊; things: gardeners' mistake; offend queen, lead to trial

Fake flowers. 2 white roses. ◊character['gardener-five] and ◊character['gardener-seven] painting them red.

◊; gardener's mistake
◊char['alice]
◊action{Sneak up on the gardeners.}

◊char['gardener-seven]
◊dialog{Five? Don't splash paint on me like that!}

◊char['gardener-five]
◊dialog{What? I didn't—}

◊char['alice]
◊dialog{Excuse me, would you tell me why you are painting those roses?}

◊char['gardener-five]
◊action{Stare at Seven.}

◊char['gardener-seven]
◊dialog{Well, you see, the Queen ordered to have red roses planted here, but we put a white one in by mistake.}
◊dialog{If Her Majesty finds out, we'd be beheaded, so, we're doing our best, before she comes, to—}

◊char['queen]
◊enter['queen]

◊char['gardener-five]
◊action{See Queen coming, panic.}
◊dialog{The Queen! The Queen!}
◊action{Lie down.}

◊char['gardener-seven]
◊action{Lie down.}

◊; Queen finds out.
◊char['queen]
◊action{Move up to Alice & gardeners.}
◊dialog{What's your name, child?}

◊char['alice]
◊dialog{◊note{politely} My name is Alice, so please your Majesty.}

◊char['queen]
◊action{Point at the gardeners.}
◊dialog{And who are these?}

◊char['alice]
◊dialog{How should ◊i{I} know? It's no business of mine.}

◊char['queen]
◊action{Show anger.}
◊dialog{◊note{angrily} I, the Queen of Hearts, order your immediate execution!}

◊char['alice]
◊dialog{What? Nonsense!}

◊char['queen]
◊dialog{◊note{to the gardeners} Get up! What have ◊i{you} been doing here?}
◊action{Examine a white rose.}

◊char['gardener-seven]
◊dialog{We were trying to—}

◊char['queen]
◊dialog{I see, I see… You two, are sentenced for execution as well!}
◊action{Drag the Gardeners off stage.}
◊action{Come back for Alice.}
◊dialog{To show the rule of law, Alice, I order you to head for the trial for your sin!}
◊action{Drag Alice away.}

◊scene{Trial.} ◊; (83%?)
◊; needs a dance in here somehow.
◊; Gardeners are gone.
◊; Trial for Alice.
◊; Queen as judge, Mouse and Rabbit present.

Judge = Queen, jury = Mouse and Cheshire Cat.

1 table for judge, 1 table for Mouse and Cheshire Cat, 1 chair for Alice.

◊char['queen]
◊dialog{I have summoned you, ◊character['cheshire-cat] and ◊character['mouse], as the jury for this trial against ◊character['alice].}
◊dialog{What do you two know of ◊character['alice] as a person?}

◊char['mouse]
◊dialog{She mocked the great goddess who created the lake of tears, and intruded into my great friend, ◊character['march-hare]'s tea party. She is truly sinful.}

◊char['alice]
◊dialog{That's not true! The lake of tears came from my tears before I shrunk—}

◊char['queen]
◊dialog{Silence! Stop your sinful mocking of the great goddess of tears!}
◊dialog{Cat, what about your opinion?}

◊char['cheshire-cat]
◊dialog{◊note{smile} She's mad.}

◊char['queen]
◊dialog{So that means you ought to be executed, Alice.}

◊char['alice]
◊dialog{What sort of nonsense are you all saying?! I'm not mad, and I'm not guilty of anything!}

◊char['queen]
◊dialog{No more complaints! Come with me!}
◊action{Reach for Alice in attempt to drag her off stage.}

◊char['alice]
◊dialog{You're all ridiculous! I'm not mad, you are—}

◊char{Stage}
◊action{Lights off.}

◊scene{Wake up.}

Alice on the chair from scene 1, Alice's sister next to her.

◊char['sister]
◊dialog{Wake up, Alice! You've been sleeping for so long!}

◊char['alice]
◊dialog{…Oh, I've had such a curious dream!}

◊char['sister]
◊dialog{What sort of dream was it?}

◊char['alice]
◊dialog{So I saw this Rabbit going by our house, and…}

◊section{End}
