#lang pollen
◊(require racket/dict)
◊(page-number)
◊title{Alice’s adventures in wonderland adaptation script}

◊note{◊link["http://www.gutenberg.org/ebooks/11"]{Original text (public domain)}}

The play should be 18 ~ 22 minutes.

Everyone must have about the same number of lines. A person can have more than 1 roles.

No narrator.

◊current-characters{
◊hasheq[
'alice "Alice"
'rabbit "White Rabbit"
'queen "Queen of Hearts"
'cheshire-cat "Cheshire Cat"
'five "5 of Hearts"
'butterfly "Butterfly"
'hatter "The Mad Hatter"
'three "3 of Hearts"
]
}

◊h2{Objects}

◊ul{
◊-{White rose ✕ 2}
◊x{Trees ✕ 4}
◊x{Huge Mushroom}
◊x{Bushes ✕ 3 (woods, field, garden)}
◊x{(Classroom property) Speech desk}
◊x{(School property) Tea party table}
◊x{(School property) Trial bench}
}

◊h2{Props}

◊ul{
◊x{Small Mushroom}
◊-{Cups ✕ 3}
◊-{Roses ✕ 2}
◊x{Paint brushes ✕ 2}
◊x{Bucket}
◊x{法槌}
}

◊section{Characters}

Kisaragi:

◊ul{
◊desc-item["Alice"]{
Present in every scene.

Needs to hold onto the small mushroom for 5 scenes.

Outfit: light-colored dress.
}
}

Elona:

◊ul{
◊desc-item["White Rabbit"]{
Leads Alice into Wonderland. Hosts the tea party.

Outfit: rabbit ears, rabbit tail.
}
◊desc-item["Queen of Hearts"]{
Hosts the trial. Scolds the gardeners.

Outfit: crown.
}
}

◊; hard-coded
◊(page-number)

Mary:

◊ul{
◊desc-item["Cheshire Cat"]{
Leads Alice to the tea party.

Outfit: cat ears.
}
◊desc-item["Five"]{
Jury 2; scolded in garden for planting wrong color of roses.

Outfit: poker cards, front and back.
}
}

Andrew:

◊ul{
◊desc-item["Butterfly"]{
Gives mushroom to Alice.

Outfit: wings.
}
◊desc-item["The Mad Hatter"]{
Tea party guest.

Outfit: hat.
}
◊desc-item["Three"]{
Jury 1; scolded in garden for planting wrong color of roses.

Has more lines than Five.

Outfit: Same as Five (with different pattern)
}
}

◊(page-number)
◊h2{Environments}

◊ul{
◊environment['home "Field"]{
Starting point.

Chair on one side, brown bushes in front; tree leading to the woods.
}
◊environment['woods "Woods"]{
Where the small mushroom is given & Cheshire Cat smiles.

◊image[#:width "30%"]{images/woods.png}

Mushroom in the center, another tree leading to the party.
}

◊environment['tea-party "Tea party"]{
Table in the center, with ◊character['hatter] and ◊character['rabbit] sitting by it. One empty seat for Alice.

Has another tree leading to Queen's garden. Trees covering the computer in the classroom.

We can use the white tableset that's always outside the classroom.
}
◊environment['garden "Queen's garden"]{
Bushes and a tree.

◊character['five], ◊character['three] painting Roses with Bushes, with a bucket next to them.
}
◊environment['trial "Trial"]{

A table facing the audience with '◊character['five] and ◊character['three] sitting by it.

Queen standing by the teacher's speech desk.
}
}

◊scene{A field with woods.}

Alice is sitting on a chair.

◊char['alice]

◊action{Doze off.}

◊enter['rabbit #:action "Running"]

◊char['rabbit]

◊action{Looks at watch.}

◊dialog{Oh dear! I'm going to be late!}

◊char['alice]

◊dialog{How curious, that rabbit has a watch!}

◊dialog{I wonder where it's going… Hey, where are you going?}

◊char['rabbit]

◊dialog{Oh my ears, I'm out of time!}

◊leave['rabbit #:action "Running"]
◊leave[#:action "Chase after White Rabbit" 'alice]

◊scene{In the woods.}

◊character['butterfly] is standing next to the huge mushroom, on the side closer to the tea party.

◊image{images/woods.png}

◊enter['alice #:action "from the field"]

◊char['alice]
◊dialog{Where is the Rabbit, and where am I?}

◊action{Walk up to the huge mushroom.}
◊dialog{What is this…?}
◊action{Stand tiptoe and peak over the huge mushroom. Stare at ◊character['butterfly].}

◊char['butterfly]
◊action{Stare at ◊character['alice] for a few seconds.}
◊dialog{Who are you?}

◊char['alice]
◊dialog{I'm like myself but not really myself—}

◊char['butterfly]
◊dialog{I do not understand a word you're saying. What is your name?}

◊char['alice]
◊dialog{I'm Alice, and, like, I think I fell into this world and I feel so small, and I don't really have any idea---}

◊; hard coded
◊(page-number)
◊char['butterfly]
◊dialog{What size do you want to be?}

◊char['alice]
◊dialog{I'm not too particular about that…}

◊char['butterfly]
◊action{Silent for several seconds.}
◊dialog{One side will make you grow taller, the other will make you grow shorter.}

◊char['alice]
◊dialog{One side of what?}

◊char['butterfly]
◊dialog{Of the mushroom.}
◊action{Points to the small mushroom.}
◊leave['butterfly]

◊char['alice]
◊dialog{I wonder what that was all about… I guess I'll pick this up in case I need it.}
◊action{Put the mushroom in a pocket.}

◊sing[#:title "What a wonderful world"]{
Such a world of wonder
which I've fallen into

Mushrooms and butterflies
living in this forest

Treasures and secret items
what a nice welcome gift

Such a world of wonder
which I've fallen into
}

◊(page-number)

◊char['alice]

◊dialog{Now, where should I go?}

◊enter['cheshire-cat]

◊char['alice]
◊action{Walk near ◊character['cheshire-cat].}
◊dialog{Excuse me, do you know which way I ought to go from here?}

◊char['cheshire-cat]
◊dialog{◊note{smile} Good evening, Alice. That—}

◊char['alice]
◊dialog{Wait, why do you know my name?}

◊char['cheshire-cat]
◊dialog{I'm a Cheshire cat, that's why.}
◊dialog{◊note{Smiles bigger.} That depends a great deal on where you want to get to.}

◊char['alice]
◊dialog{I don't care where I—}

◊char['cheshire-cat]
◊dialog{Then it doesn't matter which way you go.}

◊char['alice]
◊dialog{Well, okay — What sort of people live around here?}

◊(page-number)

◊char['cheshire-cat]
◊dialog{◊note{points to the field} In that direction, lives a Hatter; ◊note{points toward the party} in that direction, lives a March Hare. Visit whomever you like: they're both mad.}

◊char['alice]
◊dialog{By "mad", do you mean "crazy"?}

◊char['cheshire-cat]
◊dialog{Indeed, Alice.}

◊char['alice]
◊dialog{But I don't want to meet crazy people.}

◊char['cheshire-cat]
◊dialog{There's no helping that. We're all mad here. I'm mad. You're mad.}

◊char['alice]
◊dialog{How am ◊i{I} mad?}

◊char['cheshire-cat]
◊dialog{You must be, else you wouldn't be here… Would you like to meet the Queen today?}

◊char['alice]
◊dialog{I think I'd like to, but I haven't been invited yet.}

◊char['cheshire-cat]
◊dialog{Well, you'll see her soon.}
◊leave['cheshire-cat]
◊leave[#:action "Head towards the tea party" 'alice]

◊scene{the Tea Party}

Alice walks deeper into the forest, and arrives at March Hare's place. White Rabbit and the Mad Hatter are sitting at a table, having a tea party.

◊char['alice]
◊action{Walk towards the table.}
◊dialog{Oh, they're having a tea party! Let me join!}

◊char['hatter]
◊dialog{There's no room! Go away!}

◊char['alice]
◊dialog{There ◊i{is} room!}
◊action{Sit in front of ◊character['rabbit], then stare at them for a few seconds.}
◊dialog{◊note{realized that} You're the Rabbit that went by my home!}

◊char['rabbit]
◊dialog{Indeed.}
◊dialog{◊note{In an encouraging tone.} Have some wine.}

◊char['alice]
◊action{Look around.}
◊dialog{I don't see any wine.}

◊char['rabbit]
◊dialog{◊note{Looking at the watch.} There isn't any.}

◊char['alice]
◊dialog{Then it wasn't very civil of you to offer it.}

◊(page-number)

◊char['rabbit]
◊dialog{It wasn't very civil of you to sit down without being invited.}

◊char['hatter]
◊dialog{Why don't you just shut up if you really insist on staying?}

◊char['rabbit]
◊action{Wait a few seconds.}
◊dialog{Your hair needs cutting.}

◊char['alice]
◊dialog{You should learn not to make personal remarks. It's very rude.}
◊action{Wait a few seconds.}
◊dialog{Come, we shall have some fun now! Let's start asking riddles!}

◊char['hatter]
◊dialog{Why is a raven like a writing desk?}

◊char['alice]
◊dialog{I believe I can guess that.}

◊char['rabbit]
◊dialog{Do you mean that you think you can find out the answer to it?}

◊char['alice]
◊dialog{Exactly so.}

◊char['rabbit]
◊dialog{Then you should say what you mean.}

◊(page-number)

◊char['alice]
◊dialog{I do… at least---at least I mean what I say---that's the same thing, you know.}

◊char['hatter]
◊dialog{Not the same thing at all! You might just as well say "I see what I eat" is the same as "I eat what I see"!}

◊char['rabbit]
◊dialog{You might just as well say "I like what I get" is the same as "I get what I like"!}

◊char['alice]
◊dialog{Okay, okay! It's not the same thing.}

◊char['hatter]
◊action{Wait a few seconds.}
◊dialog{On that note, what day of the month is it?}

◊char['rabbit]
◊action{Stares at the watch.}
◊action{Shakes it, then holds it to her ear.}

◊char['alice]
◊dialog{Today is… the second day of the month. (proudly presenting her memory)}
◊action{Leans over to ◊character['rabbit].}
◊dialog{What a funny watch! It doesn't tell the time at all!}

◊char['rabbit]
◊dialog{Why should it? Does your watch tell you what year it is?}

◊(page-number)

◊char['alice]
◊dialog{Of course not, but that's—.}

◊char['rabbit]
◊dialog{Exactly.}

◊char['alice]
◊dialog{But that's nonsense!}

◊char['hatter]
◊dialog{You shouldn't complain after rudely appearing out of nowhere.}

◊char['alice]
◊dialog{What? You guys are so rude!}
◊action{Gets up.}

◊char['rabbit]
◊dialog{Who's making personal remarks now?}

◊sing[#:title "Worst tea party ever"]{
Joining a tea party
only to get insulted
  (Hatter: You came out of nowhere!)

Strange nonsense here and there
nobody has any sense
  (Cheshire Cat: Well, you're mad.)

Faulting me for their unfriendliness
Worst tea party ever
  (Rabbit: How self-centric!)

Glad I've left already
}

◊leave['alice #:action "through trees into the garden"]

◊scene{Garden.}
◊; things: gardeners' mistake; offend queen, lead to trial

◊character['five] and ◊character['three] are trying to paint 2 white roses red.

◊enter['alice]

◊char['alice]
◊dialog{I'll never go there again. It's the stupidest tea party I've ever been to!}

◊dialog{◊note{look at the garden} What a nice garden!}
◊action{Sneak up on the gardeners.}

◊char['three]
◊dialog{Five? Don't splash paint on me like that!}

◊char['five]
◊dialog{What? I didn't—}

◊char['alice]
◊dialog{Excuse me, would you please tell me why you are painting those roses?}

◊char['five]
◊action{Stares at Three.}

◊char['three]
◊dialog{Well, you see, the Queen ordered red roses planted here, but we put white ones in by mistake. If Her Majesty finds out, we'd be beheaded, so, we're doing our best, before she comes, to—}

◊enter['queen]

◊(page-number)

◊char['five]
◊dialog{◊note{Sees Queen coming, panics.} The Queen! The Queen!}

◊action{Both ◊character['five] and ◊character['three] quickly try to hide behind the tree.}

◊; Queen finds out.
◊char['queen]
◊dialog{◊note{to Alice} What's your name, child?}

◊char['alice]
◊dialog{◊note{politely & curtsies} My name is Alice, so please your Majesty.}

◊char['queen]
◊dialog{◊note{Point at the gardeners.} And who are these?}

◊char['alice]
◊dialog{How should ◊i{I} know? It's no business of mine.}

◊char['queen]
◊dialog{◊note{angrily} I, the Queen of Hearts, order your immediate execution!}

◊char['alice]
◊dialog{What? Nonsense!}

◊char['queen]
◊dialog{◊note{to the gardeners} Get over here! What have ◊i{you} been doing here? ◊note{Examines a white rose.}}

◊char['three]
◊dialog{We were trying to—}

◊(page-number)

◊char['queen]
◊dialog{I see, I see… You two, are sentenced for execution as well! ◊note{to Alice} To show the rule of law, Alice, I order you to head for the trial for your sin!}

◊char['alice]
◊action{Grab the mushroom from her pocket, as if to look for a use of it.}

◊scene{Trial}

◊ul{
◊li{◊b{Judge}: Queen}
◊li{◊b{Jury}: ◊character['five] and ◊character['three].}
}

◊char['queen]
◊dialog{◊character['five] and ◊character['three], you shall be the jury for this trial against Alice. And once she is executed…}

◊char['alice]
◊dialog{But you haven't proven me guilty of anything!}
◊action{Drop the mushroom in fear, in front of Three.}

◊char['queen]
◊dialog{◊note{angry at being interrupted} You are at a trial so you must be guilty of something! …And once she is executed, then the jury shall be executed too!}

◊char['five 'three]
◊action{Nodding their heads with a panicked look on their faces.}

◊char['queen]
◊dialog{◊note{to the jury} What do you two know of Alice as a person?}

◊char['five 'three]
◊dialog{◊note{together, nervously} Nothing your majesty… Only there's more evidence…}

◊char['three]
◊dialog{◊note{picks up the mushroom} …this mushroom has just been picked up.}

◊char['queen]
◊dialog{◊note{slightly confused} What is it?}

◊(page-number)

◊char['five]
◊dialog{A mushroom, my majesty. One that is likely from the great forest.}

◊char['queen]
◊dialog{So she stole a mushroom from our Great Forest… those are known to tell the truth about whomever picked it up. What does it say?}

◊char['three]
◊dialog{It's a poem. ◊note{Reading it.}}

◊blockquote{
Brainmatter of morons taints roses white;
Blood of thieves taints violets red.
}

◊char['five 'three]
◊dialog{◊note{together} She is truly sinful.}

◊char['alice]
◊dialog{That's not true! A butterfly gave it to me, I didn't steal it!}

◊char['queen]
◊dialog{◊note{hitting the gavel} Silence! Stop your sinful mocking of this trial with your shouting lies! Jury, what about your opinions?}

◊char['five 'three]
◊dialog{◊note{together} She's mad.}

◊char['queen]
◊dialog{So that's an execution! ◊note{Hit the gavel.}}

◊char['alice]
◊dialog{What sort of nonsense are you all saying?! I'm not mad, and I'm not guilty of anything!}

◊(page-number)

◊char['queen]
◊dialog{Come with me!}

◊char['alice]
◊dialog{You're all ridiculous! You are all just a bunch of playing cards!}

◊char['queen 'five 'three]
◊action{Start to swirl and twirl around Alice}

◊char['alice]
◊action{Join the others and go around in circles, then spread out.}

◊leave['queen 'five 'three]
◊leave['alice #:action "returning to field from Scene 1"]

◊scene{Wake up.}

Alice on the chair from scene 1.

◊char['alice]
◊dialog{…Oh, I've had such a curious dream! I saw this Rabbit running by me, and…}

◊h3{Scene 6.5: Final Dance}

◊(end)
