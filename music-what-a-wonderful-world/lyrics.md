Such a world of wonder
which I've fallen into

Mushrooms and butterflies
living in this forest

Treasures and secret items
what a nice welcome gift

Such a world of wonder
which I've fallen into
